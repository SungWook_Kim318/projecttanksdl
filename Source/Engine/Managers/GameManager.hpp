//
//  GameManager.hpp
//  playground
//
//  Created by kmu-lab on 11/07/2018.
//

#pragma once



class GameManager {
private:
  static GameManager* psGM;
  
  GameManager(void);
  GameManager(const GameManager&) = delete;
  const GameManager& operator=(const GameManager&) = delete;

  static constexpr int c_basicWidth = 800;
  static constexpr int c_basicHeight = 600;

  bool m_isThrow;
public:
  GameManager& operator() (void);
  static GameManager& instance(void);
  
  void Init(unsigned w = c_basicWidth, unsigned h = c_basicHeight) noexcept;
  void OnStart(void);
  void OnUpdate(void);
  void OnClose(void);
  void Destroy(void);

  void SetExceptionMode(bool turnOn);
  bool GetExceptionMode(void);
};
