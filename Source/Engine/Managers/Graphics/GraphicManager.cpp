// Engine Lib
#include "GraphicManager.hpp"

// Require Lib
#include <GL/glew.h>
#include <GLFW/glfw3.h>
// Standard Lib
#include <iostream>



GraphicManager* GraphicManager::psGM = new GraphicManager();

GraphicManager::GraphicManager()
  : m_isInit(false), m_mainVer(0), m_minorVer(0)
{ 
}


GraphicManager::~GraphicManager()
{ 
}
GraphicManager & GraphicManager::instance(void)
{
  return *psGM;
}

void GraphicManager::Init(void) noexcept
{ 
  if(m_mainVer == 0)
  {
    std::cout << "Wrong initialized version on GraphicManager";
    throw("wrong");
  }
    
  //Init glew
  glewExperimental = true; // Needed for core profile
  if(glewInit() != GLEW_OK)
  {
    fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
    getchar();
    glfwTerminate();
    throw("wrong");
  }

  // Test depth
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_CULL_FACE);
}

void GraphicManager::OnStart(void)
{ }

void GraphicManager::OnUpdate(void)
{ }

void GraphicManager::OnClose(void)
{ }

void GraphicManager::Destroy(void)
{ }

void GraphicManager::SetGraphicLibVersion(unsigned main, unsigned minor)
{ 
  m_mainVer = main, m_minorVer = minor;
}
