#pragma once

class GraphicManager
{
private:
  GraphicManager();
  ~GraphicManager();

  static GraphicManager* psGM;

  bool m_isInit = false;
  unsigned m_mainVer;
  unsigned m_minorVer;
  //TODO : TODO list
  //std::vector<u* Model> m_models;
  //std::vector<u* shaderCode> m_shdaerCodes;
  //std::vector<u* shader> m_shdaers;
  //std::list<u* Camera> m_Cams;
  //Camera* m_currentCam;
public:
  static GraphicManager& instance(void);

  void Init() noexcept;
  void OnStart(void);
  void OnUpdate(void);
  void OnClose(void);
  void Destroy(void);
  
  void SetGraphicLibVersion(unsigned main, unsigned minor);
};

