//
//  GameManager.cpp
//  playground
//
//  Created by kmu-lab on 11/07/2018.
//

#include "GameManager.hpp"

#include "Applications/ApplicationManager.hpp"
#include "Graphics/GraphicManager.hpp"



GameManager* GameManager::psGM = new GameManager();


GameManager::GameManager(void)
//:
{
  
}
GameManager& GameManager::operator()(void)
{
  return *psGM;
}
GameManager& GameManager::instance(void)
{
  return *psGM;
}
void GameManager::Init(unsigned w, unsigned h) noexcept
{
  ApplicationManager& AM = ApplicationManager::instance();
  AM.Init(w, h);
  GraphicManager& GrM = GraphicManager::instance();
  GrM.Init();
}
void GameManager::OnStart(void)
{
  
}
void GameManager::OnUpdate(void)
{
  
}
void GameManager::OnClose(void)
{
  
}
void GameManager::Destroy(void)
{
  
}

void GameManager::SetExceptionMode(bool turnOn)
{
  m_isThrow = turnOn;
}

bool GameManager::GetExceptionMode(void)
{
  return m_isThrow;
}
