// Engine Lib
#include "InputManager.hpp"
#include "ApplicationManager.hpp"
// Require Lib
//#include <GL/glew.h>
#include <GLFW/glfw3.h>
// Standard Lib
//#include <iostream>
#include <bitset>

InputManager* InputManager::psIM = new InputManager();

//TODO: Think about how can be more smarter.
extern GLFWwindow* ::g_win;

// ---------------------- GLFW Callback function ----------------------
//TODO: Make Input manager and clearing
void _CB_HandleInput(GLFWwindow *window);
void _CB_Mouse(GLFWwindow* window, double xpos, double ypos);
void _CB_MouseScroll(GLFWwindow* window, double xoffset, double yoffset);
// --------------------------------------------------------------------

InputManager::InputManager()
{
  
}

InputManager::~InputManager()
{ }
void InputManager::reset(void)
{
  m_mouse.deltaX = 0;
  m_mouse.deltaY = 0;
  m_mouse.x = 0;
  m_mouse.y = 0;
  m_mouse.wheelX = 0;
  m_mouse.wheelY = 0;
  m_mouse.mouse = KeyMap::Mouse::None;
  m_mouse.keyboard = KeyMap::Keyboard::None;
  m_Press.reset();
  m_Release.reset();
}
void InputManager::Init() noexcept
{
  reset();
    /*
  glfwSetCursorPosCallback(s_win, Mouse_CB);
  glfwSetScrollCallback(s_win, MouseScroll_CB);
  glfwSetInputMode(s_win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  */

}
void InputManager::OnStart()
{

}
void InputManager::OnUpdate()
{
  
}
void InputManager::OnClose()
{
  
}
void InputManager::Destroy()
{
  
}
InputManager::MouseData InputManager::GetMouseData(void)
{
  return m_mouse;
}

/*
//
// Callback Function
// TODO: Think about position of winsize Callback func.

//TODO: Move Input manager.
void HandleInput_CB(GLFWwindow *window)
{
  
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
  float cameraSpeed = 2.5f * deltaTime; // adjust accordingly
  if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    cameraPos += cameraSpeed * cameraFront;
  if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    cameraPos -= cameraSpeed * cameraFront;
  if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
  if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}
// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void Mouse_CB(GLFWwindow* window, double xpos, double ypos)
{
  if(firstMouse)
  {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
  lastX = xpos;
  lastY = ypos;

  float sensitivity = 0.1f; // change this value to your liking
  xoffset *= sensitivity;
  yoffset *= sensitivity;

  yaw += xoffset;
  pitch += yoffset;

  // make sure that when pitch is out of bounds, screen doesn't get flipped
  if(pitch > 89.0f)
    pitch = 89.0f;
  if(pitch < -89.0f)
    pitch = -89.0f;

  glm::vec3 front;
  front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
  front.y = sin(glm::radians(pitch));
  front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
  cameraFront = glm::normalize(front);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset)
{
  if(fov >= 1.0f && fov <= 45.0f)
    fov -= yoffset;
  if(fov <= 1.0f)
    fov = 1.0f;
  if(fov >= 45.0f)
    fov = 45.0f;
}

*/