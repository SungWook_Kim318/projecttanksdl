#pragma once

#include <KeyMap.hpp>

class InputManager
{
public:
  struct MouseData
  {
    double x,y;
    double wheelX, wheelY;
    double deltaX, deltaY;
    KeyMap::Keyboard keyboard;
    KeyMap::Mouse mouse;
  };

private:
  InputManager(void);
  ~InputManager(void);
  
  void reset(void);

  static InputManager* psIM;
  
  MouseData m_mouse;
  std::bitset<256U> m_Press;
  std::bitset<256U> m_Release;
public:
  static InputManager& instance(void);
  
  void Init(void) noexcept;
  void OnStart(void);
  void OnUpdate(void);
  void OnClose(void);
  void Destroy(void);
  
  MouseData GetMouseData(void);
};
