#pragma once



class ApplicationManager
{
private:
  ApplicationManager();
  ~ApplicationManager();

  static ApplicationManager* psAM;

  bool m_isInit;
public:
  static ApplicationManager& instance(void);

  void Init(unsigned w, unsigned h) noexcept;
  void OnStart(void);
  void OnUpdate(void);
  void OnClose(void);
  void Destroy(void);

  unsigned GetAppHeight(void);
  unsigned GetAppWidth(void);
};

