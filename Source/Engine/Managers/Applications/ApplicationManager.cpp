// Engine Lib
#include "ApplicationManager.hpp"
#include "../Graphics/GraphicManager.hpp"

// Require Lib
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>

// Standard Lib
#include <iostream>

namespace ApplicationAttrib
{
  static constexpr unsigned c_basicWidth = 800;
  static constexpr unsigned c_basicHeight = 600;

  static unsigned s_w = c_basicWidth;
  static unsigned s_h = c_basicHeight;

}
namespace
{
  SDL_Window* g_win = nullptr;
}

ApplicationManager* ApplicationManager::psAM = new ApplicationManager();


ApplicationManager::ApplicationManager()
  : m_isInit(false)
{ 
}


ApplicationManager::~ApplicationManager()
{ 
}
ApplicationManager & ApplicationManager::instance(void)
{
  return *psAM;
}

void ApplicationManager::Init(unsigned w, unsigned h) noexcept
{ 
  if( SDL_Init( SDL_INIT_VIDEO ) != 0 )// 0 is Success
  {
    printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
    m_isInit = false;
    return;
  }
  m_isInit = true;

  //macOS setting
  //OpenGL 4.1
#ifdef __APPLE__
  constexpr unsigned MajorVersion = 4;
  constexpr unsigned MinorVersion = 1;
  SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, MajorVersion );
  SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, MinorVersion );

#else
  //Other setting
  //OpenGL 4.3
  constexpr unsigned MajorVersion = 4;
  constexpr unsigned MinorVersion = 3;
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#endif
  GraphicManager::instance().SetGraphicLibVersion(MajorVersion, MinorVersion);
  SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

  //Create Process and Window
  ApplicationAttrib::s_w = w;
  ApplicationAttrib::s_h = h;

  g_win = SDL_CreateWindow( 
    "SDL Tutorial", 
    SDL_WINDOWPOS_UNDEFINED, 
    SDL_WINDOWPOS_UNDEFINED, 
    w, h, 
    SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
  if( g_win == NULL )
  {
    printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
    m_isInit = false;
    return;
  }
/*
  //Create Process and Window
  ApplicationAttrib::s_w = w;
  ApplicationAttrib::s_h = h;
  
  g_win = glfwCreateWindow(ApplicationAttrib::s_w, ApplicationAttrib::s_h, "TankEngine", NULL, NULL);
  if(g_win == NULL)
  {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    throw("");/// SHOULD NOT exist Error on make program
  }
  //Add Callback function to change Window Size
  //Make Make GL Context
  glfwMakeContextCurrent(g_win);

  // tell GLFW to capture our mouse
  glfwSetFramebufferSizeCallback(g_win, _CB_winSize);
*/
}

void ApplicationManager::OnStart(void)
{ }

void ApplicationManager::OnUpdate(void)
{ }

void ApplicationManager::OnClose(void)
{ }

void ApplicationManager::Destroy(void)
{ }

unsigned ApplicationManager::GetAppHeight(void)
{
  return ApplicationAttrib::s_h;
}

unsigned ApplicationManager::GetAppWidth(void)
{
  return ApplicationAttrib::s_w;
}